<?php
session_start();

?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Document</title>
</head>
<body>
    <!-- afficher le nombre de produit dans le pannier -->
   <a href="pannier.php" class="link">pannier<span><?=array_sum($_SESSION["panier"])?></span></a> 
    <section class="product_list">
        <?php
        //inclure la page de connexion
        include_once "conx_db.php";
        //afficher la liste des produits
        $req= mysqli_query($con,"SELECT * FROM panier");
        while($row = mysqli_fetch_assoc($req)){

        ?>
        <form action=""class="product">
            <div class="images_product">
                <img src="images/<?= $row['img']?>">

            </div>
            <div class="content">
                <h4 class="name"><?= $row['name']?></h4>
                <h2 class="price"><?= $row['prix']?>$</h2>
                <a href="ajouter_pannier.php?id=<?= $row['id']?>" class="id_produit">Ajouter au pannier</a>
            </div>
        </form>
        <?php } ?>
    </section>
</body>
</html>